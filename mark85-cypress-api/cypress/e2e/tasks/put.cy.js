describe('PUT /tasks/:id/done', () => {

    beforeEach(function () {
        cy.fixture('tasks/put').then(function (tasks) {
            this.tasks = tasks;
        });
    });

    it('update task to done', function () {

        const { user, task } = this.tasks.update

        cy.task('removeTask', task.user, user.email)
        cy.task('removeUser', user.email)
        cy.postUser(user)

        cy.postSession(user).then(respUser => {

            const token = respUser.body.token

            cy.postTask(task, token)
                .then(respTask => {

                    const taskId = respTask.body._id

                    cy.putTaskDone(taskId, token)
                        .then(response => {
                            expect(response.status).to.eq(204)
                        })

                    cy.getUniqueTask(taskId, token)
                        .then(response => {
                            expect(response.body.is_done).to.be.true
                        })
                })
        })

    });

    it('task not found', function () {

        const { user, task } = this.tasks.not_found

        cy.task('removeTask', task.user, user.email)
        cy.task('removeUser', user.email)
        cy.postUser(user)

        cy.postSession(user).then(respUser => {

            const token = respUser.body.token

            cy.postTask(task, token)
                .then(respTask => {

                    const taskId = respTask.body._id

                    cy.deleteTask(taskId, token).then(response => {
                        expect(response.status).to.eq(204)
                    })

                    cy.putTaskDone(taskId, token)
                        .then(response => {
                            expect(response.status).to.eq(404)
                        })
                })
        })

    });

});