describe('DELETE /tasks/:id', () => {

    beforeEach(function () {
        cy.fixture('tasks/delete').then(function (tasks) {
            this.tasks = tasks;
        });
    });

    it('delete task', function () {

        const { user, task } = this.tasks.remove

        cy.task('removeTask', task.user, user.email)
        cy.task('removeUser', user.email)
        cy.postUser(user)

        cy.postSession(user).then(respUser => {

            const token = respUser.body.token

            cy.postTask(task, token)
                .then(respTask => {

                    const taskId = respTask.body._id

                    cy.deleteTask(taskId, token).then(response => {
                        expect(response.status).to.eq(204)
                    })
                })
        })

    });

    it('task not found', function () {

        const { user, task } = this.tasks.not_found

        cy.task('removeTask', task.user, user.email)
        cy.task('removeUser', user.email)
        cy.postUser(user)

        cy.postSession(user).then(respUser => {

            const token = respUser.body.token

            cy.postTask(task, token)
                .then(respTask => {

                    const taskId = respTask.body._id

                    cy.deleteTask(taskId, token).then(response => {
                        expect(response.status).to.eq(204)
                    })

                    cy.deleteTask(taskId, token).then(response => {
                        expect(response.status).to.eq(404)
                    })
                })
        })

    });

});

