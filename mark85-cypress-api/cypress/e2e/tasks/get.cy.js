describe('GET /tasks', () => {

    beforeEach(function () {
        cy.fixture('tasks/get').then(function (tasks) {
            this.tasks = tasks;
        });
    });

    it('get tasks', function () {

        const { user, tasks } = this.tasks.list

        cy.task('removeTasksLike', 'Estud4r')
        cy.task('removeUser', user.email)
        cy.postUser(user)
        cy.postSession(user).then(respUser => {

            const token = respUser.body.token

            // cy.postTask(tasks[0], token)
            // cy.postTask(tasks[1], token)
            // cy.postTask(tasks[2], token)

            // melhor prática
            tasks.forEach(function (t) {
                cy.postTask(t, token)
            })

            cy.getTasks(token).then(response => {
                expect(response.status).to.eq(200)
            }).its('body')
                .should('be.an', 'array')
                .and('have.length', tasks.length)

        })

    });

    describe('GET /tasks/:id', function () {

        beforeEach(function () {
            cy.fixture('tasks/get').then(function (tasks) {
                this.tasks = tasks;
            });
        });

        it('get task', function () {

            const { user, task } = this.tasks.unique

            cy.task('removeTask', task.user, user.email)
            cy.task('removeUser', user.email)
            cy.postUser(user)

            cy.postSession(user).then(respUser => {

                const token = respUser.body.token

                cy.postTask(task, token)
                    .then(respTask => {

                        const taskId = respTask.body._id

                        cy.getUniqueTask(taskId, token).then(response => {
                            expect(response.status).to.eq(200)
                        })
                    })
            })

        });

        it('task not found', function () {

            const { user, task } = this.tasks.not_found

            cy.task('removeTask', task.user, user.email)
            cy.task('removeUser', user.email)
            cy.postUser(user)

            cy.postSession(user).then(respUser => {

                const token = respUser.body.token

                cy.postTask(task, token)
                    .then(respTask => {

                        const taskId = respTask.body._id

                        cy.deleteTask(taskId, token).then(response => {
                            expect(response.status).to.eq(204)
                        })

                        cy.getUniqueTask(taskId, token).then(response => {
                            expect(response.status).to.eq(404)
                        })
                    })
            })

        });

    });

});

Cypress.Commands.add('getTasks', (token) => {
    cy.api({
        url: '/tasks',
        method: 'GET',
        headers: {
            authorization: token
        },
        failOnStatusCode: false
    }).then(response => { return response })
})

Cypress.Commands.add('getUniqueTask', (taskId, token) => {
    cy.api({
        url: '/tasks/' + taskId,
        method: 'GET',
        headers: {
            authorization: token
        },
        failOnStatusCode: false
    }).then(response => { return response })
})

Cypress.Commands.add('deleteTask', (taskId, token) => {
    cy.api({
        url: '/tasks/' + taskId,
        method: 'DELETE',
        headers: {
            authorization: token
        },
        failOnStatusCode: false
    }).then(response => { return response })
})

